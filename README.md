# ics-ans-role-vm-owner

Ansible role to setup sudo for host owners.

Users from the `vm_owner` list are given sudo access.
If the list is empty, the file `/etc/sudoers.d/vm_owner` is removed.

## Role Variables

```yaml
# List of users to add to the vm_owner sudoers file
vm_owner: []
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-vm-owner
```

## License

BSD 2-clause
