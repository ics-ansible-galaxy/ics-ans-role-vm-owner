import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_vm_owner(host):
    vm_owner = host.file("/etc/sudoers.d/vm_owner")
    if host.ansible.get_variables()["inventory_hostname"] == "sudo-default":
        assert vm_owner.exists
        assert vm_owner.contains("root  ALL=(ALL) ALL")
    elif host.ansible.get_variables()["inventory_hostname"] == "sudo-empty-vm-owner":
        assert not vm_owner.exists
